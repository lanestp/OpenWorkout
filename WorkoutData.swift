//
//  WorkoutData.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 4/24/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation
/*
 {
 "title" : "Test",
 "type" : "interval",
 "loops" : "3",
 "pace" : [
 {"title" : "slow",
 "speed" : "3.0",
 "playlist" : "Renaissance Fair"},
 {"medium" : ""},
 {"fast" : "Lane Runageddon",
 "speed" : "7.5"
 }
 ],
 "workout" : [
 [
 {"time" : "60",
 "pace" : "slow"},
 {"time" : "240",
 "pace" : "fast",
 "description" : "7.5 MPH",
 "songs" : [
 {
 "song" : "The Ninth Wave",
 "album" : "Beyond the Red Mirror",
 "artist" : "Blind Guardian"
 }
 ]
 }
 ]
 ]
 }

 {
 "title" : "Outside Run",
 "type" : "run",
 "pace" : [
 {"title" : "run","fast" : "Lane Runageddon",
 "speed" : "7.5"
 }
 ],
 "workout" : [
 [
 {"time" : "2400",
 "pace" : "run"}
 ]
 ]
 }

 {
 "title" : "Test",
 "type" : "interval",
 "loops" : "3",
 "pace" : [
 {"title" : "slow",
 "speed" : "3.0",
 "playlist" : "Renaissance Fair"},
 {"fast" : "Lane Runageddon",
 "speed" : "7.5"
 }
 ],
 "workout" : [
 [
 {"time" : "60",
 "pace" : "slow"},
 {"time" : "240",
 "pace" : "fast"
  }
 ]
 ]
 }

 */



public class OpenWorkout : AnyObject {
    let workoutCollection:WorkoutCollection?
    let pace:Pace?
    let title:String?
    let workoutType:WorkoutType
    var loops:Int?
    var currentWorkout:Int
    var currentWorkoutObject:WorkoutObject?
    
    public func titleForIndex(index:Int) -> String?{
        switch index {
        case 0:
            return title
        case 1:
            switch workoutType {
            case .interval:
                if let loop = loops {
                    return "\(loop) "
                }
                return nil
            case .run:
                return workoutCollection![0]![0]?.time.toTimeString()
            }
        case 2:
            if let p = pace {
                return (p.pace(forIndex: 0)!.speed != nil) ? "\(p.pace(forIndex: 0)!.speed!) MPH" : nil
            }
            return nil
        case 3:
            switch workoutType {
                case .interval:
                    return "\(workoutCollection![0]![0]?.pace!.speed!)"
                default:
                    return nil//currentWorkoutObject.titleForIndex(index)
            }
        case 4:
            switch workoutType {
            case .interval:
                return workoutCollection![0]![0]?.time.toTimeString()
            default:
                return nil//currentWorkoutObject.titleForIndex(index)
            }
        case 5:
            switch workoutType {
            case .interval:
                var p = workoutCollection![0]![1]
                return "\(workoutCollection![0]![1]?.pace!.speed!)"
            default:
                return nil//currentWorkoutObject.titleForIndex(index)
            }
        case 6:
            switch workoutType {
            case .interval:
                return workoutCollection![0]![1]?.time.toTimeString()
            default:
                return nil//currentWorkoutObject.titleForIndex(index)
            }
        default:
            return nil
        }
    }
    
    public func nextForIndex(index:Int) -> String?{
        switch index {
        case 0:
            break;
        case 1:
            if loops != nil {
                loops! += 1
            }
            break;
        case 2:
            if let p = pace {
                if let pce = p.pace(forIndex: 0) {
                    if (pce.speed! > 3.01){
                        pce.speed! -= 0.1
                    }
                }
            }
            break;
        default:
            break;
        }
        return titleForIndex(index)
    }
    
    public func previousForIndex(index:Int) -> String?{
        switch index {
        case 0:
            break;
        case 1:
            if loops != nil && 1 < loops!{
                loops! -= 1
            }
            break;
        case 2:
            if let p = pace {
                if let pce = p.pace(forIndex: 0) {
                    if (pce.speed! > 3.01){
                        pce.speed! -= 0.1
                    }
                }
            }
            break;
        case 4:
            if let p = pace {
                if let pce = p.pace(forIndex: 1) {
                    pce.speed! -= 0.1
                }
            }
            break;
        case 3 :
            break;
        case 5 :
            break;
        default:
            break;
        }
        return titleForIndex(index)
    }
    
    init(JSONObject:AnyObject){
        guard let contents = JSONObject as? [String : AnyObject]
        else {
            workoutCollection = nil
            pace = nil
            title = nil
            loops = nil
            currentWorkout = 0
            workoutType = .run
            return
        }
        pace = Pace(contentsIn: contents["pace"])
        workoutCollection = WorkoutCollection(contentsIn: contents["workout"],pace:pace!)
        title = contents["title"] as? String
        if let loopStr = contents["loops"] as? String {
            loops = Int(loopStr)
        }
        if let currentWorkoutTemp = contents["current"] as? String {
            if let c = Int(currentWorkoutTemp) {
                currentWorkout = c
            }
            else {
                currentWorkout = 0
            }
        }
        else {
            currentWorkout = 0
        }
        
        if let WorkoutTypeTemp = contents["type"] as? String {
            switch WorkoutTypeTemp {
            case "run":
                workoutType = .run
                break
            case "interval" :
                workoutType = .interval
                break
            default:
                workoutType = .run
            }
        }
        else {
            workoutType = .run
        }
        //currentWorkoutObject = workoutCollection?[currentWorkout]

    }
    
    func StartWorkout() -> Bool{
        currentWorkoutObject = workoutCollection?.StartWorkout(currentWorkout, sectionDone: ChangeSection, completion: EndWorkout)
        print(currentWorkoutObject?.songs?.songs)
        return currentWorkoutObject != nil
    }
    func ChangeSection(newWorkoutObject:WorkoutObject) {
        print("ChangeSection \(newWorkoutObject.songs?.songs)")
        currentWorkoutObject = newWorkoutObject
    }
    func PauseWorkout() {
        //currentWorkoutObject?.pause()
    }
    func EndWorkout() {
        print("EndWorkout")
    }
    
}

public class WorkoutCollection : AnyObject{
    public let workouts:[WorkoutList]?
    var WorkoutNumber:Int = 0
    var currentWorkout:WorkoutList? = nil
    init (contentsIn:AnyObject?, pace:Pace) {
        guard let contents = contentsIn as? [[AnyObject]]
        else {
            workouts = nil
            return
        }
        var workoutsOut = [WorkoutList]()
        contents.forEach { (item) in
            workoutsOut.append(WorkoutList(contentsIn: item, pace: pace))
        }
        workouts = workoutsOut
    }
    func StartWorkout(workoutNumber:Int, sectionDone:(newWorkoutObject:WorkoutObject) -> Void , completion:() -> Void) -> WorkoutObject? {
        WorkoutNumber = workoutNumber
        guard let workout = workouts?[WorkoutNumber]
        else {
            return nil
        }
        currentWorkout = workout
        return workout.StartWorkout(sectionDone, completion: completion)
        
    }
    subscript (index: Int) -> WorkoutList? {
        get {
            return workouts![index]
            // return an appropriate subscript value here
        }
    }
}

//collection of workouts
public class WorkoutList : AnyObject{
    let contents:[WorkoutObject]?
    var currentOffset = 0
    var completionHandler:(()->Void)?
    var sectionHandler:((newWorkoutObject:WorkoutObject)->Void)?
    init (contentsIn:[AnyObject]?, pace:Pace) {
        completionHandler = nil
        sectionHandler = nil
        if (contentsIn == nil) {
            contents = nil
            currentOffset = 0
            return
        }
        var contentsOut = [WorkoutObject]()
        contentsIn!.forEach { (item) in
            if let a = item as? [String : AnyObject] {
                contentsOut.append(WorkoutObject(value: a, pace: pace))
            }
        }
        contents = contentsOut
    }
    
    public func StartWorkout (sectionDone:(newWorkoutObject:WorkoutObject) -> Void, completion:() -> Void) -> WorkoutObject? {
        completionHandler = completion
        sectionHandler = sectionDone
        if (currentOffset < contents!.count - 1) {
            if let c = contents {
                c[currentOffset].start(completionHandlerList)
                return c[currentOffset]
            }
        }
        return nil
    }
    
    func completionHandlerList() {
        currentOffset += 1
        if (contents!.count - 1 < currentOffset) {
            if (completionHandler != nil) {
                completionHandler!()
            }
        }
        else {
            if let c = contents {
                if (sectionHandler != nil) {
                    sectionHandler!(newWorkoutObject: c[currentOffset])
                }
                c[currentOffset].start(completionHandlerList)
            }
        }
    }
    
    public var current:WorkoutObject? {
        get {
            return contents?[currentOffset]
            
        }
    }
    subscript (Index:Int) -> WorkoutObject? {
        get {
            return contents?[Index]
        }
    }
}

//Single Workout object
public class WorkoutObject : AnyObject {
    public let time:Int
    public let description:String?
    public let songs:Songs?
    var speedNum:Float?
    public var speed:String? {
        return (speedNum != nil) ? "\(speedNum)" : nil
    }
    public let pace:PaceObject?
    var timerObj:NSTimer?
    var currentTime:Int
    var timerDone:(()->Void)?

    public func setSpeed(newSpeed:Float){
        speedNum = newSpeed
    }
    
    init(value:[String : AnyObject], pace pacevar:Pace){
        if let timeStr = value["time"] as? String {
            if let timeInt = Int(timeStr) {
                time = timeInt
            }
            else {
                time = 0
            }
        }
        else {
            time = 0
        }
        
        pace = pacevar.PaceObjectForString(value["pace"] as? String)
        description = value["description"] as? String
        songs = Songs(contentsIn: value["songs"])
        currentTime = 0
        timerObj = nil
        timerDone = nil
    }
    
    @objc public func nextTick(){
        currentTime += 1
        if (currentTime >= time) {
            end()
        }
    }
    
    public func pause() {
        if let timer = timerObj {
            timer.invalidate()
        }
    }
    
    public func start(timerCallback:(()->Void)?) -> Bool{
        if (timerCallback != nil) {
            timerDone = timerCallback
        }
        if (currentTime <= time) {
            timerObj = NSTimer.scheduledTimerWithTimeInterval(1 as NSTimeInterval, target: self, selector: #selector(nextTick), userInfo: nil, repeats: true)
            return true
        }
        return false
    }
    
    public func end() {
        pause()
        if let timerDoneFunc = timerDone {
            timerDoneFunc()
        }
    }
}

//Collection(Dictionary) of paceobjects
public class Pace {
    let contents:[String : PaceObject]?
    
    init (contentsIn:AnyObject?) {
        guard let c = contentsIn as? [AnyObject]
        else {
            contents = nil
            return
        }
        var contentsOut = [String : PaceObject]()
        c.forEach({ (item) in
            let p = PaceObject(contentsIn: item)
            if ((p.title) != nil){
                contentsOut[p.title!] = p
            }
        })
        if contentsOut.count < 1 {
            contents = nil
        }
        else {
            contents = contentsOut
        }
    }
    public func PaceObjectForString(paceString:String?) -> PaceObject?{
        if let p = paceString, let c = contents{
            return c[p]
        }
        return nil
    }
    
    public func playlist(forPace forPace:String?) -> String?{
        if let c = contents, let p = forPace {
            return c[p]?.playlist
        }
        return nil
    }
    public func title(forPace forPace:String?) -> String?{
        if let c = contents, let p = forPace {
            return c[p]?.title
        }
        return nil
    }
    public func speed(forPace forPace:String?) -> Float?{
        if let c = contents, let p = forPace {
            return c[p]?.speed
        }
        return nil
    }

    public func pace(forIndex forIndex:Int) -> PaceObject?{
        if let a = orderedPace() {
            return a[forIndex]
        }
        return nil
    }
    func orderedPace() -> Array<PaceObject>? {
        if let c = contents {
            var arr = Array(c.values)
            arr.sortInPlace({ (first, second) -> Bool in
                return first.speed! < second.speed!
            })
            return arr
        }
        return nil
    }
}

//Instance of a single pace object
public class PaceObject {
    let title:String?
    let playlist:String?
    var speed:Float?
    
    init (contentsIn:AnyObject?) {
        guard let contents = contentsIn as? [String : String]
        else {
            title = nil
            playlist = nil
            speed = nil
            return
        }
        title = contents["title"]
        playlist = contents["playlist"]
        if let speedstr = contents["speed"] {
            speed = Float(speedstr)
        }
        else {
            speed = nil
        }
        
        //print("\(title!) : \(playlist!)")
    }
    
}

//Collection(Array) of songs
public class Songs {
    let songs:[Song]?
    
    init (contentsIn:AnyObject?) {
        print(contentsIn)
        guard let contents = contentsIn as? [AnyObject]
        else {
            songs = nil
            return
        }
        var contentsOut = [Song]()
        contents.forEach { (item) in
            contentsOut.append(Song(contentsIn: item))
        }
        if contentsOut.count <= 0 {
            songs = nil
        }
        else {
            songs = contentsOut
            PlayMusic.PlaySongs(self)
        }
    }
    
}
//Instance of a single song
public class Song :CustomStringConvertible{
    let song:String?
    let artist:String?
    let album:String?
    init (contentsIn:AnyObject?) {
        guard let contents = contentsIn as? [String : String]
            else{
                song = nil
                artist = nil
                album = nil
                return
            }
        song = contents["song"]
        artist = contents["artist"]
        album = contents["album"]
        print ("\(song) \(artist) \(album)")
        //contents = contentsIn as? Dictionary<String,String>
    }
    public var description: String {
        get {return "\(song) + \(artist) : \(album)"}
    }
}

public enum WorkoutType{
    case run
    case interval
}
