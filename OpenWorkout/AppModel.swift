//
//  AppModel.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 5/2/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation
import UIKit

public class AppModel {
    static let instance = AppModel()
    public var numQuickWorkouts:Int {
        get {
            return quickWorkouts == nil ? 0 : quickWorkouts!.count
        }
    }
    var selectedWorkout:Int = 0
    var currentWorkout:OpenWorkout?
    var availableWorkouts:Array<OpenWorkout>?
    var quickWorkouts:Array<OpenWorkout>?
    
    init (){
        currentWorkout = JsonParser.ParseString("{\"title\" : \"Outside Run\",\"type\" : \"run\",\"pace\" : [{\"title\" : \"run\",\"fast\" : \"Lane Runageddon\",\"speed\" : \"7.5\"}],\"workout\" : [[{\"time\" : \"2400\",\"pace\" : \"run\"}]]}")
        availableWorkouts = Array<OpenWorkout>()
        availableWorkouts?.append(currentWorkout!)
        availableWorkouts?.append(currentWorkout!)

        quickWorkouts = Array<OpenWorkout>()
        quickWorkouts?.append(currentWorkout!)
        quickWorkouts?.append(JsonParser.ParseString("{\"title\" : \"Test\",\"Interval\" : \"interval\",\"loops\" : \"3\",\"pace\" : [{\"title\" : \"slow\",\"speed\" : \"3.0\",\"playlist\" : \"Renaissance Fair\"},{\"fast\" : \"Lane Runageddon\",\"speed\" : \"7.5\"}],\"workout\" : [[{\"time\" : \"60\",\"pace\" : \"slow\"},{\"time\" : \"240\",\"pace\" : \"fast\"}]]}")!)
        //quickWorkouts?.append(JsonParser.ParseString(" {\"title\" : \"Interval\",\"loops\" : \"3\",\"type\" : \"interval\",\"pace\" : [{\"title\" : \"slow\",\"speed\":\"3.0\",\"playlist\" : \"Renaissance Fair\"},{\"medium\" : \"\"},{\"fast\" : \"Lane Runageddon\",\"speed\":\"7.5\"}],\"workout\" : [[{\"time\" : \"60\",\"pace\" : \"slow\"},{\"time\" : \"240\",\"pace\" : \"fast\",\"description\" : \"7.5 MPH\",\"songs\" : [{\"song\" : \"Battle (2)\",\"album\" : \"Heroes of Might & Magic II Soundtrack\",\"artist\" : \"New World Computing\"}]}]]}")!)
    }
    
    public func LeftAction(index index:Int) {
        if (index == 0) { //switch workout
            if (numQuickWorkouts < 2) {
                return
            }
            else if let av = quickWorkouts where (0 < selectedWorkout){
                selectedWorkout -= 1
                currentWorkout = av[selectedWorkout]
            }
            else if let av = quickWorkouts{
                selectedWorkout = av.count-1
                currentWorkout = av[selectedWorkout]
            }
        }
        else {
            currentWorkout?.previousForIndex(index)
        }
        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "UpdateMainUI", object: nil))
    }
    public func RightAction(index index:Int) {
        if (index == 0) { //switch workout
            if (numQuickWorkouts < 2) {
                return
            }
            else if let av = quickWorkouts where (selectedWorkout < numQuickWorkouts-1){
                selectedWorkout += 1
                currentWorkout = av[selectedWorkout]
            }
            else if let av = quickWorkouts {
                selectedWorkout = 0
                currentWorkout = av[selectedWorkout]
            }
        }
        else {
            currentWorkout?.nextForIndex(index)
        }
        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "UpdateMainUI", object: nil))
    }
    public func MainAction(index index:Int) {
        print(index)
        if (index == 0) { //switch workout
            let alert = UIAlertController(title: "Choose a Workout", message: nil, preferredStyle: .ActionSheet)
            var i = 0
            quickWorkouts?.forEach({ (item) in
                let j = i
                i += 1
                alert.addAction(
                    UIAlertAction(title: item.title, style: .Default, handler: { (action) in
                        self.currentWorkout = item
                        self.selectedWorkout = j
                        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "UpdateMainUI", object: nil))
                    })
                )
            })
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion:nil) 
        }
    }
    
    public func Title(index index:Int) -> String? {
        return currentWorkout?.titleForIndex(index)
    }
}