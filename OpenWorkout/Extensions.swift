//
//  Extensions.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 4/24/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation

extension String {
    func localizedWithComment(comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: comment)
    }
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
    func padding(length: Int) -> String {
        return self.stringByPaddingToLength(length, withString: " ", startingAtIndex: 0)
    }
    
    func padding(length: Int, paddingString: String) -> String {
        return self.stringByPaddingToLength(length, withString: paddingString, startingAtIndex: 0)
    }
}

extension Int {
    func toTimeString() -> String{
        
        let hour:Int = self/3600
        let minute:Int = self/60
        let second:Int = self%60
        let secondstr = String(format: "%02d", second)
        if hour > 0 {
            let minutestr = String(format: "%02d", minute)
            return "\(hour):\(minutestr):\(secondstr)"
        }
        else if minute > 0 {
            return "\(minute):\(secondstr)"
        }
        else {
            return "\(second)"
        }

    }
}