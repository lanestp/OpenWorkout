//
//  JsonParser.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 4/24/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation
/*
 {
	"title" : "Test",
	"pace" : [
 {"slow" : "Renaissance Fair"},
 {"medium" : },
 {"fast" : "Lane Runageddon"}
	],
	"workout" : [
 {"Workout 1" : [
 {"1" : "slow"},
 {"4" : "fast",
 }
 ]}
	]
 }
 */
public class JsonParser {
    public static func ParseString(json:String) -> OpenWorkout?{
        let jsondata = (json as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        do {
            let jsonout = try NSJSONSerialization.JSONObjectWithData(jsondata!, options: .AllowFragments)
            let dict = jsonout as! Dictionary<String, AnyObject>
            //let arr = dict["workout"] as! Array<AnyObject>
            return OpenWorkout(JSONObject: dict)
            //workout.StartWorkout()
            //print (workout)
        }
        catch {
            return nil
        }
    }
    //public let title:String?
    //public let pace:Pace?
    //public let workout:WorkoutData?
    init (){
//        let jsonstr = " {\"title\" : \"Test\",\"pace\" : [{\"title\" : \"slow\",\"playlist\" : \"Renaissance Fair\"},{\"medium\" : \"\"},{\"fast\" : \"Lane Runageddon\"}],\"workout\" : [[{\"time\" : \"60\",\"pace\" : \"slow\"},{\"time\" : \"240\",\"pace\" : \"fast\",\"description\" : \"7.5 MPH\",\"songs\" : [{\"song\" : \"Battle (2)\",\"album\" : \"Heroes of Might & Magic II Soundtrack\",\"artist\" : \"New World Computing\"}]}]]}"
//        let jsondata = (jsonstr as NSString).dataUsingEncoding(NSUTF8StringEncoding)
//        do {
//            let jsonout = try NSJSONSerialization.JSONObjectWithData(jsondata!, options: .AllowFragments)
//            let dict = jsonout as! Dictionary<String, AnyObject>
//            //let arr = dict["workout"] as! Array<AnyObject>
//            let workout = OpenWorkout(JSONObject: dict)
//            workout.StartWorkout()
//            print (workout)
//        }
//        catch {
//            
//        }
    }
    /*init (json:String) {
        guard let NSJsonString = (json as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        else {
            title = ""
            pace = nil
            workout = nil
            return
        }
        do {
            let jsonObject = try NSJSONSerialization.JSONObjectWithData(NSJsonString, options: .AllowFragments)
            guard let jsonDict = jsonObject as? Dictionary<String, AnyObject>
                else {
                    title = ""
                    pace = nil
                    workout = nil
                    return
                }
            title = jsonDict["title"] as? String
            workout = WorkoutData(contentsIn: jsonDict["workout"] as? Array<AnyObject>)
            pace = Pace(contentsIn: jsonDict["pace"])
        }
        catch {
            title = ""
            pace = nil
            workout = nil
            return
        }
    }
    
    public func LoadNewJson(fileName:String) -> AnyObject?{
        guard let contents = NSData(contentsOfFile: fileName)
            else { return nil}
        do {
            let parsed = try NSJSONSerialization.JSONObjectWithData(contents, options: .AllowFragments)
            print(parsed)
            return parsed
        }
        catch {return nil}
    }*/

}
