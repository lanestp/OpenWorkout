//
//  WorkoutAdjustItemVC.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 5/2/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation
import UIKit


public class WorkoutAdjustmentVC: UIViewController {
    @IBOutlet var StepLeft:UIButton?
    @IBOutlet var StepRight:UIButton?
    @IBOutlet var TextButton:UIButton?
    @IBInspectable var Index:Int = 0
    var leftTimer:NSTimer?
    var rightTimer:NSTimer?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        if let sl = StepLeft {
            sl.addTarget(self, action: #selector(leftButtonPressed), forControlEvents: UIControlEvents.TouchDown)
            sl.addTarget(self, action: #selector(leftButtonReleased), forControlEvents: UIControlEvents.TouchUpInside)
            sl.addTarget(self, action: #selector(leftButtonReleased), forControlEvents: UIControlEvents.TouchCancel)
            sl.addTarget(self, action: #selector(leftButtonReleased), forControlEvents: UIControlEvents.TouchDragExit)
            sl.addTarget(self, action: #selector(leftButtonPressed), forControlEvents: UIControlEvents.TouchDragEnter)
        }
        if let sr = StepRight {
            sr.addTarget(self, action: #selector(rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
            sr.addTarget(self, action: #selector(rightButtonReleased), forControlEvents: UIControlEvents.TouchUpInside)
            sr.addTarget(self, action: #selector(rightButtonReleased), forControlEvents: UIControlEvents.TouchCancel)
            sr.addTarget(self, action: #selector(rightButtonReleased), forControlEvents: UIControlEvents.TouchDragExit)
            sr.addTarget(self, action: #selector(rightButtonPressed), forControlEvents: UIControlEvents.TouchDragEnter)
        }
        if let b = TextButton {
            b.addTarget(self, action: #selector(mainButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(refresh), name: "UpdateMainUI", object: nil)
        refresh()
    }
    public func refresh() {
        let title = AppModel.instance.Title(index: Index)
        if title == nil || title == "" {
            TextButton?.hidden = true;
            StepLeft?.hidden = true;
            StepRight?.hidden = true;
        }
        else {
            TextButton?.hidden = false;
            StepLeft?.hidden = false;
            StepRight?.hidden = false;
            //TextButton?.enabled = false
            TextButton?.setTitle(AppModel.instance.Title(index: Index), forState: UIControlState.Normal)
            //TextButton?.enabled = true
        }
    }
    override public func viewWillAppear(animated: Bool) {
        actionDone(true)
    }
    internal func mainButtonPressed () {
        AppModel.instance.MainAction(index: Index)
    }
    
    internal func leftButtonPressed () {
        leftTimer = NSTimer.scheduledTimerWithTimeInterval(0.25 as NSTimeInterval, target: self, selector: #selector(leftButtonHeld), userInfo: nil, repeats: true)
        leftButtonHeld()
    }
    internal func leftButtonHeld () {
        AppModel.instance.LeftAction(index: Index)
    }
    internal func leftButtonReleased () {
        leftTimer?.invalidate()
    }
    func actionDone(result:Bool){
        //print ("down")
    }

    internal func rightButtonPressed () {
        rightTimer = NSTimer.scheduledTimerWithTimeInterval(0.25 as NSTimeInterval, target: self, selector: #selector(leftButtonHeld), userInfo: nil, repeats: true)
        rightButtonHeld()
    }
    internal func rightButtonHeld () {
        AppModel.instance.RightAction(index: Index)
    }
    internal func rightButtonReleased () {
        rightTimer?.invalidate()
    }
}