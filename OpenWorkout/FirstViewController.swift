//
//  FirstViewController.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 4/24/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func remoteControlReceivedWithEvent(event: UIEvent?) { // *
        let rc = event!.subtype
        //let p = self.player.player
        print("received remote control \(rc.rawValue)") // 101 = pause, 100 = play
//        switch rc {
//        case .RemoteControlTogglePlayPause:
//            if p.playing { p.pause() } else { p.play() }
//        case .RemoteControlPlay:
//            p.play()
//        case .RemoteControlPause:
//            p.pause()
//        default:break
//        }
        
    }

}

