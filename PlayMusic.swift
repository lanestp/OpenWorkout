//
//  PlayMusic.swift
//  OpenWorkout
//
//  Created by Lane Thompson on 4/27/16.
//  Copyright © 2016 Lane Thompson. All rights reserved.
//

import Foundation
import MediaPlayer
import AVKit

public class PlayMusic {
    static var session:AVAudioSession = AVAudioSession.sharedInstance()
    static var av:AVPlayer?
    static var item:AVPlayerItem?
    static var songCount:Int = -1
    static var songUrl:NSURL?
    static var songs:Songs?
    static var onceToken: dispatch_once_t = 0
    
    public static func PlaySongs(songsVar:Songs) {
        //dispatch_once(&onceToken, {
        //})
        
        songs = songsVar
        PlaySong(nil)
    }
    
    @objc public static func PlaySong(notification:NSNotification?){
        return
        guard let song = songs
        else {
            return
        }
        songCount += 1
        
        let q = MPMediaQuery.songsQuery()
        var filter:MPMediaPropertyPredicate
        if let thesongs = song.songs where song.songs?.count > 0 {
            if thesongs.count <= songCount {
                songCount = 0
            }
            if let title = thesongs[songCount].song {
                filter = MPMediaPropertyPredicate(value: title, forProperty: MPMediaItemPropertyTitle)
                q.addFilterPredicate(filter)
            }
            if let album = thesongs[songCount].album {
                filter = MPMediaPropertyPredicate(value: album, forProperty: MPMediaItemPropertyAlbumTitle)
                q.addFilterPredicate(filter)
            }
            if let artist = thesongs[songCount].artist {
                filter = MPMediaPropertyPredicate(value: artist, forProperty: MPMediaItemPropertyArtist)
                q.addFilterPredicate(filter)
            }
            print(q.items)
            if let itemarr = q.items where 0 <= q.items?.count {
                if let url = itemarr[0][MPMediaItemPropertyAssetURL] as? NSURL {
                    songUrl = url
                    
                    MPMusicPlayerController.systemMusicPlayer().beginGeneratingPlaybackNotifications()
                    MPMusicPlayerController.systemMusicPlayer().prepareToPlay()
                    MPMusicPlayerController.systemMusicPlayer().setQueueWithItemCollection(q.collections![0])
                    MPMusicPlayerController.systemMusicPlayer().play()
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaybackStateChanged), name:
                        MPMusicPlayerControllerPlaybackStateDidChangeNotification
                        , object: MPMusicPlayerController.systemMusicPlayer())

                    /*NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: item)
                    item = AVPlayerItem(URL: songUrl!)
                    av = AVPlayer(playerItem: item!)
                    av!.play()
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaySong), name: AVPlayerItemDidPlayToEndTimeNotification, object: item)
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaybackFailed), name: AVPlayerItemFailedToPlayToEndTimeNotification, object: item)*/
                    //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaySong), name: AVPlayerItemDidPlayToEndTimeNotification, object: item)
                    //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaySong), name: AVPlayerItemDidPlayToEndTimeNotification, object: item)
                }
            }
        }
    }
    @objc public static func PlaybackStateChanged(notification:NSNotification?){
        print ("bacon")
        //av!.play()
    }
    public static func PlayPlaylist(playlist:String) {
        
    }
    
}